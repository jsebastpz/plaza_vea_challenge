import 'package:dvl_plaza_vea_challenge/util/constants.dart';
import 'package:dvl_plaza_vea_challenge/util/my_shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'components/input_wrapper.dart';

class RegisterScreen extends StatefulWidget {

  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => RegisterScreenState();
}


class RegisterScreenState extends State<RegisterScreen> {

  @override
  Widget build(BuildContext context) {

    double wsize = MediaQuery.of(context).size.width;
    double hsize = MediaQuery.of(context).size.height;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: Container(
        width: wsize,
        height: hsize,
        child: Expanded(
          child: Column(
            children: <Widget>[
              const Flexible(
                  flex: 2,
                  child: SizedBox(height: 100,)),
              Image.asset(
                'assets/images/plaza_vea_logo.png',
                width: 200,
              ),
              const Flexible(
                  flex: 2,
                  child: Padding(padding: EdgeInsets.only(top: 20, bottom: 40),
                    child: Text(
                      'Regístrate para Ingresar',
                      style: TextStyle(color: kPrimaryColor, fontFamily: 'Poppins', fontWeight: FontWeight.w700),
                    ),)),
              const Flexible(
                  flex: 15,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 30),
                    child: InputWrapper(),)),
              Align(
                    alignment: Alignment.bottomLeft,
                    child: Image.asset(
                      'assets/images/present.png',
                      width: 200,
                    ),
                  ),
            ],
          ),
        )
      ),
    );
  }
}