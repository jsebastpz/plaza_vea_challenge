import 'package:dvl_plaza_vea_challenge/network/response/offer_response.dart';
import 'package:json_annotation/json_annotation.dart';

part 'offer_list_response.g.dart';

@JsonSerializable()
class OfferListResponse{

  OfferListResponse(this.items);

  @JsonKey(name: "items")
  List<OfferResponse> items;

  factory OfferListResponse.fromJson(Map<String, dynamic> json) => _$OfferListResponseFromJson(json);
  Map<String, dynamic> toJson() => _$OfferListResponseToJson(this);
}