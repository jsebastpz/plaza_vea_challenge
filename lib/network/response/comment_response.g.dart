// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'comment_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CommentResponse _$CommentResponseFromJson(Map<String, dynamic> json) =>
    CommentResponse(
      json['text'] as String,
      UserResponse.fromJson(json['user'] as Map<String, dynamic>),
      json['id'] as String,
      json['postId'] as String,
    );

Map<String, dynamic> _$CommentResponseToJson(CommentResponse instance) =>
    <String, dynamic>{
      'text': instance.text,
      'user': instance.user,
      'id': instance.id,
      'postId': instance.postId,
    };
