import 'package:dvl_plaza_vea_challenge/network/response/comment_response.dart';
import 'package:dvl_plaza_vea_challenge/network/response/offer_list_response.dart';
import 'package:dvl_plaza_vea_challenge/network/service/offer_service.dart';
import 'package:dvl_plaza_vea_challenge/screens/components/comment_list_item.dart';
import 'package:dvl_plaza_vea_challenge/screens/components/offer_detail.dart';
import 'package:dvl_plaza_vea_challenge/util/constants.dart';
import 'package:flutter/material.dart';

import 'offer_list_item.dart';

class CommentList extends StatefulWidget{

  String offerId;

  CommentList({Key? key, required this.offerId}) : super(key: key);

  @override
  State<StatefulWidget> createState() => new CommentListState();

}


class CommentListState extends State<CommentList> {

  @override
  void initState() {
    getComments();
    super.initState();
  }

  Future<List<CommentResponse>> getComments() async {

    return OfferService().getCommentsByOfferId(widget.offerId);
  }

  @override
  Widget build(BuildContext context) {

    return FutureBuilder(
      future: getComments(),
      builder: (BuildContext context, AsyncSnapshot<List<CommentResponse>> snapshot) {
        if(snapshot.hasData) {
          return ListView.builder(
              itemCount: snapshot.data!.length,
              itemBuilder: (BuildContext context, int index){
                return Center(child: CommentListItem(commentResponse: snapshot.data![index]),
                );
              });
        } else {
          return SizedBox(
            width: 5e00,
            height: 10,
            child: Container(
                color: kTextFieldColor
            ),
          );
        }
      },
    );
  }
}