// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'offer_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OfferResponse _$OfferResponseFromJson(Map<String, dynamic> json) =>
    OfferResponse(
      json['image'] as String,
      json['createdAt'] as String,
      json['description'] as String,
      json['title'] as String,
      UserResponse.fromJson(json['user'] as Map<String, dynamic>),
      json['id'] as String,
    );

Map<String, dynamic> _$OfferResponseToJson(OfferResponse instance) =>
    <String, dynamic>{
      'image': instance.image,
      'createdAt': instance.createdAt,
      'description': instance.description,
      'title': instance.title,
      'user': instance.user,
      'id': instance.id,
    };
