// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'offer_list_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OfferListResponse _$OfferListResponseFromJson(Map<String, dynamic> json) =>
    OfferListResponse(
      (json['items'] as List<dynamic>)
          .map((e) => OfferResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$OfferListResponseToJson(OfferListResponse instance) =>
    <String, dynamic>{
      'items': instance.items,
    };
