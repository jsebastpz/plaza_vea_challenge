import 'package:shared_preferences/shared_preferences.dart';

class MySharedPreferences{

  static const _keyNombres= 'nombres';

  static const _keyApellidos= 'apellidos';


  //getters


  Future<String> getNombres() async {
    final SharedPreferences _prefs = await SharedPreferences.getInstance();

    String? nombres = _prefs.getString(_keyNombres);

    return nombres ?? '';

  }

  Future<String> getApellidos() async {
    final SharedPreferences _prefs = await SharedPreferences.getInstance();

    String? apellidos = _prefs.getString(_keyApellidos);

    return apellidos ?? '';

  }


  //setters

  Future<bool> setRegister(String nombres, String apellidos) async {
    final SharedPreferences _prefs = await SharedPreferences.getInstance();

    _prefs.setString(_keyNombres, nombres);
    _prefs.setString(_keyApellidos, apellidos);

    return _prefs.commit();
  }


}