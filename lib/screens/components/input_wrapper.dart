import 'dart:io';

import 'package:dvl_plaza_vea_challenge/screens/home_screen.dart';
import 'package:dvl_plaza_vea_challenge/util/constants.dart';
import 'package:dvl_plaza_vea_challenge/util/my_shared_preferences.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';

class InputWrapper extends StatefulWidget {

  const InputWrapper({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => InputWrapperState();

}

class InputWrapperState extends State<InputWrapper>{
  File? image;
  final _formKey = GlobalKey<FormState>();

  String nombres = "", apellidos = "";

  doRegister() async{
    await MySharedPreferences().setRegister(nombres, apellidos);
    Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen()),);
  }

  Future pickImage() async {
    try{
      final image = await ImagePicker().pickImage(source: ImageSource.gallery);
      if(image == null) return;
      final imageTemporary = File(image.path);
      setState(() {
        this.image = imageTemporary;
      });
    } on PlatformException catch(e){
      print("Failed to pick image: $e");
    }

  }

  @override
  Widget build(BuildContext context) {
    double wsize = MediaQuery.of(context).size.width;
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(padding: EdgeInsets.symmetric(vertical: 5),
              child: Text("Foto de perfil", style: TextStyle(fontWeight: FontWeight.w700, fontFamily: 'Poppins'),)),
          Padding(padding: EdgeInsets.symmetric(vertical: 5),
                  child: Align(
                    alignment: Alignment.center,
                    child: GestureDetector(
                      onTap: () => pickImage(),
                      child: ClipOval(
                        child: Align(
                          alignment: Alignment.center,
                          heightFactor: wsize/450,
                          widthFactor: wsize/450,
                          child: image != null ? Image.file(image!, height: wsize/3, width: wsize/3,) : SvgPicture.asset("assets/images/default_photo.svg", height: wsize/3,),
                        ),
                      ),
                    ),
                  ),),
          const Padding(padding: EdgeInsets.symmetric(vertical: 5),
                  child: Text("Nombres", style: TextStyle(fontWeight: FontWeight.w700, fontFamily: 'Poppins'),)),
          Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
            child: TextFormField(
                onChanged: (text){
                  nombres = text;
                },
                decoration: InputDecoration(
                    filled: true,
                    fillColor: kTextFieldColor,
                    contentPadding: EdgeInsets.symmetric(horizontal: 30),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: const BorderSide(
                        width: 0,
                        style: BorderStyle.none,
                      ),
                    ),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Debe ingresar al menos un nombre';
                  }
                  else{
                    return null;
                  }
                },
              ),),
          const Padding(padding: EdgeInsets.symmetric(vertical: 5),
              child: Text("Apellidos", style: TextStyle(fontWeight: FontWeight.w700, fontFamily: 'Poppins'),)),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: TextFormField(
                onChanged: (text){
                  apellidos = text;
                },
                decoration: InputDecoration(
                  filled: true,
                  fillColor: kTextFieldColor,
                  contentPadding: EdgeInsets.symmetric(horizontal: 30),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(
                      width: 0,
                      style: BorderStyle.none,
                    ),
                  ),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Debe ingresar al menos un apellido';
                  }
                  else{
                    return null;
                  }
                },
              ),),
          Padding(padding: EdgeInsets.symmetric(vertical: 10),
          child: ElevatedButton(
            onPressed: (){
              if (_formKey.currentState!.validate()) {
                doRegister();
              }else{
                print("Not Validated");
              }
            },
            style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
                minimumSize: Size.fromHeight(35),
                primary: kSecondaryColor,
                elevation: 0,
                padding: EdgeInsets.symmetric(horizontal: 100, vertical: 10),
                textStyle: TextStyle(fontSize: 15)
            ),
            child: const Text("Ingresar", style: TextStyle(color: kPrimaryColor, fontWeight: FontWeight.w700, fontFamily: 'Poppins'),),
          ),)
        ],
      ),
    );
  }

}