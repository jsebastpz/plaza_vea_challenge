import 'package:dvl_plaza_vea_challenge/network/response/comment_response.dart';
import 'package:dvl_plaza_vea_challenge/network/response/offer_response.dart';
import 'package:dvl_plaza_vea_challenge/util/constants.dart';
import 'package:flutter/material.dart';

class CommentListItem extends StatefulWidget{

  CommentResponse commentResponse;

  CommentListItem({Key? key, required this.commentResponse}) : super(key: key);

  @override
  State<StatefulWidget> createState() => new CommentListItemState();

}

class CommentListItemState extends State<CommentListItem> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double wsize = MediaQuery.of(context).size.width;
    double hsize = MediaQuery.of(context).size.height;
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: wsize/12 ,vertical: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Padding(padding: EdgeInsets.only(top: hsize/50),
                child: Align(
                    alignment: Alignment.center,
                    child: Positioned.fill(
                        child: Align(
                          alignment: Alignment.bottomLeft,
                          child:  ClipOval(
                            child: Image.network(widget.commentResponse.user.avatar, fit: BoxFit.fill, height: hsize/20, width: hsize/20,),
                          ),
                        ))
                ),),
                Padding(padding: EdgeInsets.only(bottom: hsize/90, left: hsize/90),
                  child: Text(widget.commentResponse.user.firstName + " " + widget.commentResponse.user.lastName, style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w500, fontSize: wsize/30, color: kTextColor),),),
              ],
            ),
            Padding(padding: EdgeInsets.only(left: hsize/18),
              child: Text(widget.commentResponse.text,
                style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w100, fontSize: wsize/30, color: kTextColor),),),
          ],
        )
    ) ;
  }
}