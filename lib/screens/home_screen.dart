import 'package:dvl_plaza_vea_challenge/util/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'components/offer_list.dart';

class HomeScreen extends StatefulWidget{

  HomeScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => new HomeScreenState();

}


class HomeScreenState extends State<HomeScreen> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double wsize = MediaQuery.of(context).size.width;
    double hsize = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: wsize/20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
                padding: EdgeInsets.only(top: hsize/15, bottom: hsize/30),
                child: Align(
                  alignment: Alignment.center,
                  child: Image.asset("assets/images/plaza_vea_logo.png", height: hsize/20,),
                ),),
            Padding(
              padding: EdgeInsets.only(bottom: hsize/30),
              child: Align(
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Text("TODAS LAS PROMOCIONES", style: TextStyle(fontSize: wsize/28, fontWeight: FontWeight.w700, fontFamily: 'Poppins', color: kPrimaryColor),),
                    Divider(
                      color: kPrimaryColor,
                      height: 25,
                      thickness: 2,
                      indent: wsize/3,
                      endIndent: wsize/3,
                    ),
                  ],
                ),
              ),),
            Padding(padding: EdgeInsets.only(left: wsize/15, top: hsize/60, bottom: hsize/100), child: Text("Ultimas promociones", style: TextStyle(fontSize: wsize/23, fontWeight: FontWeight.w700, fontFamily: 'Poppins'),),),
            Expanded(child: Padding(padding: EdgeInsets.symmetric(horizontal: 20), child:  Container(height: hsize, child: OfferList(),))),
          ],
        ),
      ),
    );
  }
}