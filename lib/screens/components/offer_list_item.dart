import 'package:dvl_plaza_vea_challenge/network/response/offer_response.dart';
import 'package:dvl_plaza_vea_challenge/util/constants.dart';
import 'package:flutter/material.dart';

class OfferListItem extends StatefulWidget{

  OfferResponse offerResponse;

  OfferListItem({Key? key, required this.offerResponse}) : super(key: key);

  @override
  State<StatefulWidget> createState() => new OfferListItemState();

}

class OfferListItemState extends State<OfferListItem> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double wsize = MediaQuery.of(context).size.width;
    double hsize = MediaQuery.of(context).size.height;
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: wsize/12 ,vertical: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Align(
              alignment: Alignment.center,
              child: Stack(
                children: [
                  Container(
                    width: wsize/1.4,
                    height: hsize/6,
                    decoration: BoxDecoration(
                      color: kTextFieldColor,
                      image: DecorationImage(
                        image: NetworkImage(widget.offerResponse.image),
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.all( Radius.circular(10.0)),
                      border: Border.all(
                        color: kTextFieldColor,
                        width: 1.5,
                      ),
                    ),
                  ),
                  Positioned.fill(
                      child: Align(
                        alignment: Alignment.bottomLeft,
                        child:  ClipOval(
                          child: Image.network(widget.offerResponse.user.avatar, fit: BoxFit.fill, height: hsize/20, width: hsize/20,),
                        ),
                      ))
                ],
              ),
            ),
            Padding(padding: EdgeInsets.symmetric(vertical: 10),
                    child: Text(widget.offerResponse.title, style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w500, fontSize: wsize/28, color: kTextColor),),),
            Padding(padding: EdgeInsets.symmetric(vertical: 10),
              child: Text(widget.offerResponse.description,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w100, fontSize: wsize/28, color: kTextColor),),),
          ],
        )
    ) ;
  }
}