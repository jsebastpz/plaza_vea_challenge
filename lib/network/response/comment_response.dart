import 'package:dvl_plaza_vea_challenge/network/response/user_response.dart';
import 'package:json_annotation/json_annotation.dart';

part 'comment_response.g.dart';

@JsonSerializable()
class CommentResponse{

  CommentResponse(this.text, this.user, this.id, this.postId);

  @JsonKey(name: "text")
  String text;

  @JsonKey(name: "user")
  UserResponse user;

  @JsonKey(name: "id")
  String id;

  @JsonKey(name: "postId")
  String postId;

  factory CommentResponse.fromJson(Map<String, dynamic> json) => _$CommentResponseFromJson(json);
  Map<String, dynamic> toJson() => _$CommentResponseToJson(this);
}