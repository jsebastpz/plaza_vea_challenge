import 'package:dvl_plaza_vea_challenge/network/response/offer_list_response.dart';
import 'package:dvl_plaza_vea_challenge/network/service/offer_service.dart';
import 'package:dvl_plaza_vea_challenge/screens/components/offer_detail.dart';
import 'package:dvl_plaza_vea_challenge/util/constants.dart';
import 'package:flutter/material.dart';

import 'offer_list_item.dart';

class OfferList extends StatefulWidget{

  OfferList({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => new OfferListState();

}


class OfferListState extends State<OfferList> {
  final ScrollController _scrollController = ScrollController();
  int items = 10;
  int page = 1;

  @override
  void initState() {
    getOffers();
    super.initState();
    _scrollController.addListener(() {
      if(_scrollController.position.pixels >= _scrollController.position.maxScrollExtent){
        setState(() {
          items += 10;
          getOffers();
        });
      }
    });
  }

  Future<OfferListResponse> getOffers() async {

    return OfferService().getOffers(page, items);

  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {



    return FutureBuilder(
      future: getOffers(),
      builder: (BuildContext context, AsyncSnapshot<OfferListResponse> snapshot) {
        if(snapshot.hasData) {
          return ListView.builder(
              controller: _scrollController,
              itemCount: snapshot.data!.items.length,
              itemBuilder: (BuildContext context, int index){
                return Center(child: GestureDetector(
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => OfferDetail(offerResponse: snapshot.data!.items[index])),);
                  },
                  child: OfferListItem(offerResponse: snapshot.data!.items[index]),
                ),
                );
              });
        } else {
          return SizedBox(
            width: 600,
            height: 10,
            child: Container(
                color: kTextFieldColor
            ),
          );
        }
      },
    );
  }
}