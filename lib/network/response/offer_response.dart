import 'package:dvl_plaza_vea_challenge/network/response/user_response.dart';
import 'package:json_annotation/json_annotation.dart';

part 'offer_response.g.dart';

@JsonSerializable()
class OfferResponse{

  OfferResponse(this.image, this.createdAt, this.description, this.title, this.user, this.id);

  @JsonKey(name: "image")
  String image;

  @JsonKey(name: "createdAt")
  String createdAt;

  @JsonKey(name: "description")
  String description;

  @JsonKey(name: "title")
  String title;

  @JsonKey(name: "user")
  UserResponse user;

  @JsonKey(name: "id")
  String id;

  factory OfferResponse.fromJson(Map<String, dynamic> json) => _$OfferResponseFromJson(json);
  Map<String, dynamic> toJson() => _$OfferResponseToJson(this);
}