import 'package:dvl_plaza_vea_challenge/network/response/offer_response.dart';
import 'package:dvl_plaza_vea_challenge/screens/components/comment_list.dart';
import 'package:dvl_plaza_vea_challenge/util/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';


class OfferDetail extends StatefulWidget{

  OfferResponse offerResponse;

  OfferDetail({Key? key, required this.offerResponse}) : super(key: key);

  @override
  State<StatefulWidget> createState() => new OfferDetailState();

}


class OfferDetailState extends State<OfferDetail> {

  String fecha = "";
  String hour = "";
  String published = "";

  @override
  void initState() {
    setPublished();
    super.initState();
  }

  setPublished(){
    setState(() {
      DateTime publishedDay =  DateTime.parse(widget.offerResponse.createdAt);
      hour = DateFormat('hh:mma').format(publishedDay);
      published = "Publicado a las " + hour;
    });
  }

  @override
  Widget build(BuildContext context) {
    double wsize = MediaQuery.of(context).size.width;
    double hsize = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Expanded(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: wsize/20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Stack(
                      children: [
                        Padding(padding: EdgeInsets.only(top: hsize/12, bottom: hsize/40, left: wsize/40),
                        child: IconButton(onPressed:() { Navigator.pop(context);}, icon: Icon(CupertinoIcons.arrow_left)),),
                        Padding(
                          padding: EdgeInsets.only(top: hsize/15, bottom: hsize/40),
                          child: Align(
                            alignment: Alignment.center,
                            child: Image.asset("assets/images/plaza_vea_logo.png", height: hsize/20,),
                          ),)
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: hsize/30, horizontal: wsize/15),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Column(
                          children: [
                            Text(widget.offerResponse.title, style: TextStyle(fontSize: wsize/25, fontWeight: FontWeight.w700, fontFamily: 'Poppins', color: kTextColor),),
                          ],
                        ),
                      ),),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        width: wsize/1.3,
                        height: hsize/5.5,
                        decoration: BoxDecoration(
                          color: kTextFieldColor,
                          image: DecorationImage(
                            image: NetworkImage(widget.offerResponse.image),
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.only( topRight: Radius.circular(10.0), topLeft: Radius.circular(10.0), bottomLeft: Radius.circular(30.0), bottomRight: Radius.circular(10.0)),
                          border: Border.all(
                            color: kTextFieldColor,
                            width: 1.5,
                          ),
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
                      child: Row(children: [
                        Align(
                          alignment: Alignment.bottomLeft,
                          child:  ClipOval(
                            child: Image.network(widget.offerResponse.user.avatar, fit: BoxFit.fill, height: hsize/20, width: hsize/20,),
                          ),
                        ),
                        Padding(padding: EdgeInsets.only(left: wsize/20),child: Text(widget.offerResponse.user.firstName + " " + widget.offerResponse.user.lastName, style: TextStyle(fontSize: wsize/25, fontWeight: FontWeight.w700, fontFamily: 'Poppins'),),)
                      ],),),
                    Padding(padding: EdgeInsets.symmetric(horizontal: wsize/15), child: Text(widget.offerResponse.description, style: TextStyle(fontSize: wsize/28, fontWeight: FontWeight.w100, fontFamily: 'Poppins'),),),
                    Padding(padding: EdgeInsets.symmetric(horizontal: wsize/15, vertical: hsize/80), child: Text(published, style: TextStyle(fontSize: wsize/40, fontWeight: FontWeight.w100, fontFamily: 'Poppins'),),),
                    Padding(
                      padding: EdgeInsets.only(top: hsize/30, left: wsize/15, right: wsize/15),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Column(
                          children: [
                            Text("Comentarios", style: TextStyle(fontSize: wsize/25, fontWeight: FontWeight.w700, fontFamily: 'Poppins', color: kTextColor),),
                          ],
                        ),
                      ),),
                    Container(height: hsize/2, child: CommentList(offerId: widget.offerResponse.id,),),
                    Padding(padding: EdgeInsets.only(top: 10, left: 30, right: 30, bottom: 50),
                      child: TextFormField(
                        decoration: InputDecoration(
                          hintText: "Escriba un comentario..",
                          filled: true,
                          fillColor: kLightColor,
                          contentPadding: EdgeInsets.symmetric(horizontal: 30),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(50),
                            borderSide: const BorderSide(
                              width: 0,
                              style: BorderStyle.none,
                            ),
                          ),
                        ),
                      ),),
                  ],
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    color: kLightColor,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(30.0), topRight: Radius.circular(30.0))
                ),
                width: wsize,
                height: hsize/8,
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 30, horizontal: wsize/10),
                  child: Row(
                    children: [
                      ElevatedButton(
                        onPressed: () => showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            content: const Text('Próximamente', style: TextStyle(fontFamily: 'Poppins', color: kTextColor)),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () => Navigator.pop(context, 'OK'),
                                child: const Text('OK', style: TextStyle(fontFamily: 'Poppins', color: kPrimaryColor)),
                              ),
                            ],
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
                            primary: kTextFieldColor,
                            elevation: 0,
                            padding: EdgeInsets.symmetric(horizontal: wsize/20, vertical: 10),
                            textStyle: TextStyle(fontSize: 15)
                        ),
                        child: Row(
                          children: [
                            const Text("16", style: TextStyle(color: kTextColor, fontWeight: FontWeight.w100, fontFamily: 'Poppins'),),
                            Padding(padding: EdgeInsets.only(left: wsize/50) ,child: Icon(CupertinoIcons.heart, color: kTextColor,)),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: wsize/30),
                        child: ElevatedButton(
                        onPressed: () => showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            content: const Text('Próximamente', style: TextStyle(fontFamily: 'Poppins', color: kTextColor)),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () => Navigator.pop(context, 'OK'),
                                child: const Text('OK', style: TextStyle(fontFamily: 'Poppins', color: kPrimaryColor),),
                              ),
                            ],
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
                            primary: kPrimaryColor,
                            elevation: 0,
                            padding: EdgeInsets.symmetric(horizontal: wsize/6, vertical: 10),
                            textStyle: TextStyle(fontSize: 15)
                        ),
                        child: const Text("Reservar", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w700, fontFamily: 'Poppins'),),
                      ),)
                    ],
                  ),)
              ),
            ],
          )
        ),
      ),
    );
  }
}