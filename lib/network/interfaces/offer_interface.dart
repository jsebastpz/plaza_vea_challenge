import 'package:dvl_plaza_vea_challenge/network/response/comment_response.dart';
import 'package:dvl_plaza_vea_challenge/network/response/offer_list_response.dart';
import 'package:dvl_plaza_vea_challenge/network/response/offer_response.dart';
import 'package:dvl_plaza_vea_challenge/util/constants.dart';
import 'package:dio/dio.dart';

abstract class IOffer {

  Future<OfferListResponse> getOffers(int page, int limit) async {
    final api = baseUrl + 'v1/posts/';
    final dio = Dio();
    Response response;
    response = await dio.get(api, queryParameters: { "page": page, "limit" : limit});
    return OfferListResponse.fromJson(response.data);
  }

  Future<OfferResponse> getOfferById(String id) async {
    final api = baseUrl + 'v1/posts/' + id;
    final dio = Dio();
    Response response;
    response = await dio.get(api);
    return OfferResponse.fromJson(response.data);
  }

  Future<List<CommentResponse>> getCommentsByOfferId(String id) async {
    final api = baseUrl + 'v1/posts/' + id + "/comments";
    final dio = Dio();
    Response response;
    response = await dio.get(api);
    return (response.data as List).map((e) => CommentResponse.fromJson(e)).toList();
  }

}