import 'package:flutter/material.dart';


//Colors
const kPrimaryColor = Color(0xFFFC2016);
const kSecondaryColor = Color(0xFFFFE81E);
const kLightColor = Color(0xFFF5F5F5);
const kTextFieldColor = Color(0xFFEAEAEA);
const kTextColor = Color(0xFF434343);


//URL
const String baseUrl = "https://61c35faa9cfb8f0017a3eb2e.mockapi.io/api/";

